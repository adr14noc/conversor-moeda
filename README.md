Conversor de Moedas

Conversor de moedas em linha de comando

Getting Started

conversor-monetario codigoMoedaOrigem codigoMoedaDestino valorACalcular

Prerequisites
Python 2+

Contributing
Please read CONTRIBUTING.md for details on our code of conduct, and the process for submitting pull requests to us.

Versioning
Ver 0.1

Authors
Adriano Cardoso - Itau Unibanco

License
This project is licensed under the MIT License - see the LICENSE.md file for details



